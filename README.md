# Two ways to read Exif Data from luatex

*Method 1:* use a lua script "myexif.lua" to parse the EXIF data. This works with
LuaLaTeX in TeXLive 2018 out of the box. The lua script only gives the exposure 
information now, adding more fields requires changing the lua script. 
Corresponing TeX File is exiftest.tex

*Method 2:* use libexif. You need libexif itself and the Lua Bindings to libexif, 
which are found here: https://github.com/minoki/luaexif 
This requires LuaTeX 5.3 or later, which means in TeXLive 2018 you need to link
your luatex/lualatex to the luatex53. With libexif you can access many more 
fields, including ImageDescription and Camera Make and Model. See lua53test.tex
The luabinding exif.so needs to be either in the TeX Paths or you need to create
a link in the same folder as the TeX File.